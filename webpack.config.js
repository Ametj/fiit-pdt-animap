'use strict';
var path = require('path');
const webpack = require('webpack');


module.exports = {
    entry: [
        'react-hot-loader/patch',
        'webpack-hot-middleware/client',
        './client/client.js'
    ],
    output: {
        path: path.join(__dirname, 'public/scripts'),
        filename: 'bundle.js',
        publicPath: '/static/'
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ],
    module: {
        loaders: [
            {
                name: 'scripts',
                test: /\.js$/,
                include: path.join(__dirname, 'client'),
                loader: 'babel',
            },
            {
                name: 'styles',
                test: /\.scss$/,
                include: path.join(__dirname, 'client'),
                loaders: ['style', 'css', 'sass']
            }
        ]
    }
};