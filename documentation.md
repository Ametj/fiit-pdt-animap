# Overview

This application shows terrestrial mammals distribution on a map. Most important features are:

- search and show terrestrial mammal distribution by binomial (Colored by presence)

- search terrestrial mammals that live in given country and show their intersection

- search and show terrestrial mammals that live in given location

AniMap in action:

![Screenshot](screenshot.png)

The application has 2 separate parts, the client which is a [frontend web application](#frontend) using [React.js](https://facebook.github.io/react/), mapbox API and mapbox-gl.js and the [backend application](#backend) written in [express.js](http://expressjs.com/), backed by PostGIS. The frontend application communicates with backend using a [REST API](#api).

# Frontend

The frontend application is built with react.js, bundled by webpack into `public/scripts/bundle.js` and imported into server views.
Frontend shows a mapbox-gl.js widget and menu. It is displaying spatial distribution of terrestrial mammals, thus the map style is set to outdoors-v10 style. Polygons colors are set by `presence`.
Redux.js is used for managing state of application.

The frontend responsibilities are:

- detecting user's location, using the standard [web location API](https://developer.mozilla.org/en-US/docs/Web/API/Geolocation/Using_geolocation),

- displaying the sidebar menu tabs,

- calling appropriate backend APIs,

- displaying geo features by overlaying the map with a geojson layer, the geojson is generated in fronted from data obtained from backend APIs.

# Backend

The backend application is written in Express.js and is responsible for querying geo data based on REST API requests.

## Data

[Borders of World Countries](http://www.naturalearthdata.com/downloads/50m-cultural-vectors/50m-admin-0-countries-2/) shapefile is imported to table `countries` using `shp2pgsql-gui` tool. Using same tool are also imported spatial distribution data of [terrestrial mammals](http://www.iucnredlist.org/technical-documents/spatial-data/) into table `terrestrial_mammals`. 
By default this tool will create index for column `geom`. 

After importing data is important to run  *sql/country.sql* and *sql/mammals.sql*. It will update srid of `geom` and create column `coordinates` (geojson representation of `geom`) for both tables.
For `countries` it will also create index on `name`. For `terrestrial_mammals` it will remove redundant rows, create index on `binomial`, cluster `terrestrial_mammals` based on this index and create new table `mammals_polygons` which is dump of `terrestrial_mammals geom`. Indexes for `mammals_polygons` are created on `gid` and `geom`).

Connection string is defined in `server/database.js`

## Api

**List of all countries**

`GET /api/countries`

**List of all terrestrial mammals**

`GET /api/mammals`

**Get country coordinates by name**

`GET /api/country?name=Slovakia`

**Get mammal coordinates by binomial or id**

`GET /api/mammal?name=Panthera uncia&presence=`

**Get mammals binomial and intersections data by country (return 10 results, offset = last binomial)**

`GET /api/mammal/country?country=Slovakia&offset=`

**Get intersection of country and mammal (for performance only uses data which intersects with given country)**

`GET api/mammal/intersections?country=Slovakia&data=15275,38192,63928,78027`

**Get mammals binomial by location**

`GET /api/mammal/location?lat=49.308137&long=21.567374`
