﻿select UpdateGeometrySRID('terrestrial_mammals','geom',4326);
delete from terrestrial_mammals where gid not in (select min(gid) from terrestrial_mammals group by id_no, subspecies, subpop, geom);
alter table terrestrial_mammals add coordinates text;
update terrestrial_mammals set coordinates = (st_asgeojson(geom))::json ->> 'coordinates';
create index index_terrestrial_mammals_on_binomial on terrestrial_mammals(binomial);
cluster terrestrial_mammals using index_terrestrial_mammals_on_binomial;
vacuum analyze terrestrial_mammals;

create table mammals_polygons as select gid, (st_dump(geom)).geom geom from terrestrial_mammals;
create index index_mammals_polygons_geom on mammals_polygons using gist(geom);
create index index_mammals_polygons_gid on mammals_polygons(gid)
analyze mammals_polygons;
