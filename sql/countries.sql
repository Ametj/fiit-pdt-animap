select UpdateGeometrySRID('countries','geom',4326);
alter table countries add coordinates text;
update countries set coordinates = (st_asgeojson(geom))::json ->> 'coordinates';
create index index_countries_on_name on countries(name);
analyze countries;