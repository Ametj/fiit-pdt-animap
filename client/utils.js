'use strict';
export function fetchJson(uri, callback) {
    console.log('Going to fetch:', uri);
    fetch(uri)
        .then(response => {
                if (response.status !== 200) {
                    console.log(`Looks like there was a problem. Status Code: ${response.status}`);
                    return;
                }
                response.json().then(data => {
                    callback(data);
                });
            }
        )
        .catch(err => {
            console.log(`Fetch Error : ${err}`);
        });
}