'use strict';
import {ON_COUNTRY_CHANGE, ON_COUNTRY_OFFSET_CHANGE, GET_BY_COUNTRY, GET_BY_COUNTRY_NEXT} from '../actions/actionTypes';

const initialState = {
    value: '',
    shown: '',
    offset: 0,
    data: []
};

const byCountry = (state = initialState, action) => {
    console.log('Action:', action);
    switch (action.type) {
        case ON_COUNTRY_CHANGE:
            return {
                ...state,
                value: action.data
            };
        case ON_COUNTRY_OFFSET_CHANGE:
            return {
                ...state,
                offset: action.data
            };
        case GET_BY_COUNTRY:
            return {
                ...state,
                shown: action.shown,
                data: action.data,
                offset: 0
            };
        case GET_BY_COUNTRY_NEXT:
            return {
                ...state,
                data: [...state.data, ...action.data]
            };
        default:
            return state;
    }
};

export default byCountry;