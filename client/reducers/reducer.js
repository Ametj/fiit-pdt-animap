'use strict';
import {combineReducers} from 'redux'
import map from './mapReducer';
import byBinomial from './byBinomialReducer';
import byLocation from './byLocationReducer';
import byCountry from './byCountryReducer';

export default combineReducers({
    map,
    byBinomial,
    byCountry,
    byLocation
})