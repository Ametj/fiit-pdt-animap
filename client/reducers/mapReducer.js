'use strict';
import {SHOW_POLYGON, SHOW_MARKER} from '../actions/actionTypes';

const initialState = {
    showOnMapData: [],
    marker: []
};

export default function map(state = initialState, action) {
    switch (action.type) {
        case SHOW_POLYGON:
            return {
                ...state,
                showOnMapData: action.data
            };
        case SHOW_MARKER:
            return {
                ...state,
                marker: action.marker
            };
        default:
            return state;
    }
};