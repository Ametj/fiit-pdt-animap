'use strict';
import {ON_BINOMIAL_CHANGE} from '../actions/actionTypes';

const initialState = {
    value: ''
};

export default function byBinomial(state = initialState, action) {
    switch (action.type) {
        case ON_BINOMIAL_CHANGE:
            return {
                ...state,
                value: action.data
            };
        default:
            return state;
    }
};