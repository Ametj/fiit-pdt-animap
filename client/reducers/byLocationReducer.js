'use strict';
import {ON_LAT_CHANGE, ON_LONG_CHANGE, GET_BY_LOCATION, ON_LOCATION_OFFSET_CHANGE} from '../actions/actionTypes';

const initialState = {
    lat: '49.308137',
    long: '21.567374',
    offset: 0,
    data: []
};

export default function byLocation(state = initialState, action) {
    switch (action.type) {
        case ON_LAT_CHANGE:
            return {
                ...state,
                lat: action.data
            };
        case ON_LONG_CHANGE:
            return {
                ...state,
                long: action.data
            };
        case ON_LOCATION_OFFSET_CHANGE:
            return {
                ...state,
                offset: action.data
            };
        case GET_BY_LOCATION:
            return {
                ...state,
                data: action.data,
                offset: 0
            };
        default:
            return state;
    }
};