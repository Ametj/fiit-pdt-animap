'use strict';
import {connect} from 'react-redux'
import ByLocation from '../components/Menu/Tabs/ByLocation';
import {fetchMammal, onChange, fetchByLocation, showMarker} from '../actions/actions';
import {ON_LAT_CHANGE, ON_LONG_CHANGE, ON_LOCATION_OFFSET_CHANGE} from '../actions/actionTypes';

const mapStateToProps = state => {
    return {
        lat: state.byLocation.lat,
        long: state.byLocation.long,
        offset: state.byLocation.offset,
        data: state.byLocation.data
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onItemClick: (binomial) => {
            dispatch(fetchMammal(binomial, 1));
        },
        onLatChange: (lat) => {
            dispatch(onChange(ON_LAT_CHANGE, lat));
        },
        onLongChange: (long) => {
            dispatch(onChange(ON_LONG_CHANGE, long));
        },
        onOffsetChange: (offset) => {
            dispatch(onChange(ON_LOCATION_OFFSET_CHANGE, offset));
        },
        onSubmit: (lat, long) => {
            dispatch(showMarker([lat, long]));
            dispatch(fetchByLocation(lat, long));
        }
    };
};

const ByLocationContainer = connect(mapStateToProps, mapDispatchToProps)(ByLocation);
export default  ByLocationContainer;