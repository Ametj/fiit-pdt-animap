'use strict';
import {connect} from 'react-redux'
import Map from '../components/Map';

const mapStateToProps = state => {
    return {
        data: state.map.showOnMapData,
        marker: state.map.marker
    };
};

const MapContainer = connect(mapStateToProps)(Map);

export default  MapContainer;