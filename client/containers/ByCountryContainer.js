'use strict';
import {connect} from 'react-redux'
import ByCountry from '../components/Menu/Tabs/ByCountry';
import {
    fetchMammalByCountryIntersection,
    fetchByCountry,
    fetchByCountryNext,
    onChange,
    showMarker
} from '../actions/actions';
import {ON_COUNTRY_CHANGE, ON_COUNTRY_OFFSET_CHANGE} from '../actions/actionTypes';

const mapStateToProps = state => {
    return {
        value: state.byCountry.value,
        offset: state.byCountry.offset,
        shown: state.byCountry.shown,
        data: state.byCountry.data
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onItemClick: (country, intersections) => {
            dispatch(fetchMammalByCountryIntersection(country, intersections));
        },
        onValueChange: (country) => {
            dispatch(onChange(ON_COUNTRY_CHANGE, country));
        },
        onOffsetChange: (offset) => {
            dispatch(onChange(ON_COUNTRY_OFFSET_CHANGE, offset));
        },
        onSubmit: (country) => {
            dispatch(showMarker([]));
            dispatch(fetchByCountry(country));
        },
        loadNext: (country, offset) => {
            dispatch(fetchByCountryNext(country, offset))
        }
    };
};

const ByCountryContainer = connect(mapStateToProps, mapDispatchToProps)(ByCountry);

export default  ByCountryContainer;