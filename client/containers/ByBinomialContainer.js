'use strict';
import {connect} from 'react-redux'
import ByBinomial from '../components/Menu/Tabs/ByBinomial';
import {fetchMammal, onChange, showMarker} from '../actions/actions';
import {ON_BINOMIAL_CHANGE} from '../actions/actionTypes';

const mapStateToProps = state => {
    return {
        value: state.byBinomial.value
    };
};

const mapDispatchToProps = dispatch => {
    return {
        showOnMap: (binomial, presence) => {
            dispatch(showMarker([]));
            dispatch(fetchMammal(binomial, presence));
        },
        onChange: (binomial) => {
            dispatch(onChange(ON_BINOMIAL_CHANGE, binomial));
        }
    };
};

const ByBinomialContainer = connect(mapStateToProps, mapDispatchToProps)(ByBinomial);
export default  ByBinomialContainer;