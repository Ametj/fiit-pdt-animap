'use strict';
import React from 'react';
import Style from './App.scss';
import MapContainer from '../../containers/MapContainer';
import Menu from '../Menu';
import Footer from '../Footer';

export default class App extends React.Component {
    render() {
        return (
            <div>
                <div className='columns'>
                    <Menu />
                    <MapContainer />
                </div>
                <Footer />
            </div>
        );
    }
}