'use strict';
import React, {PropTypes} from 'react'
import AnimalItem from './AnimalItem';
import Style from './AnimalList.scss';

const AnimalList = ({items, onAnimalItemClick}) => (
    <ul className='alist'>
        {items.map(item =>
            <AnimalItem
                key={item.binomial}
                text={item.binomial}
                onClick={() => onAnimalItemClick(item)}  //
            />
        )}
    </ul>
);


AnimalList.propTypes = {
    items: PropTypes.array.isRequired,
    onAnimalItemClick: PropTypes.func.isRequired
};

export default AnimalList;