'use strict';
import React, {PropTypes} from 'react';

const AnimalItem = ({onClick, text}) => <li className='aitem' onClick={onClick}>{text}</li>;


AnimalItem.propTypes = {
    onClick: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired,
};

export default AnimalItem;