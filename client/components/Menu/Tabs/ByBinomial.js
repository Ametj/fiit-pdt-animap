'use strict';
import React, {PropTypes} from 'react';
import Autosuggest from 'react-autosuggest';
import {
    getSuggestions,
    getSuggestionValue,
    renderInputComponent,
    renderSuggestion,
    renderSuggestionsContainer
} from './Suggestion/Suggestion';
import {fetchJson} from '../../../utils';

let data = [];

export default class ByBinomial extends React.Component {
    constructor() {
        super();
        this.state = {suggestions: []};
        if (data.length === 0) fetchJson('api/mammals', json => data = json);
    }

    onChange = (event, {newValue}) => {
        this.props.onChange(newValue);
    };

    onSuggestionsFetchRequested = ({value}) => {
        this.setState({
            suggestions: getSuggestions(value, data)
        });
    };

    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    render() {
        const {suggestions} = this.state;
        const {value, showOnMap} = this.props;
        const inputProps = {
            placeholder: 'Type a binomial',
            value,
            onChange: this.onChange
        };
        return ( //TODO https://casesandberg.github.io/react-color/
            <div>
                <div className='control'>
                    <label className='label'>Binomial:
                        <Autosuggest
                            suggestions={suggestions}
                            onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                            onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                            getSuggestionValue={getSuggestionValue}
                            renderInputComponent={renderInputComponent}
                            renderSuggestion={renderSuggestion}
                            renderSuggestionsContainer={renderSuggestionsContainer}
                            inputProps={inputProps}
                        />
                    </label>
                    <button className='button is-primary' onClick={() => showOnMap(value)}>
                        Show on map
                    </button>
                </div>
                <table className='colors table'>
                    <tbody>
                    <tr>
                        <td>Extant</td>
                        <td>
                            <div style={{'backgroundColor': '#0f0', 'width': '30px', 'height': '14px'}}/>
                        </td>
                    </tr>
                    <tr>
                        <td>Possibly Extant</td>
                        <td>
                            <div style={{'backgroundColor': '#af4', 'width': '30px', 'height': '14px'}}/>
                        </td>
                    </tr>
                    <tr>
                        <td>Possibly extinct</td>
                        <td>
                            <div style={{'backgroundColor': '#f80', 'width': '30px', 'height': '14px'}}/>
                        </td>
                    </tr>
                    <tr>
                        <td>Extinct</td>
                        <td>
                            <div style={{'backgroundColor': '#f00', 'width': '30px', 'height': '14px'}}/>
                        </td>
                    </tr>
                    <tr>
                        <td>Not mapped</td>
                        <td>
                            <div style={{'backgroundColor': '#000', 'width': '30px', 'height': '14px'}}/>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}

ByBinomial.PropTypes = {
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    showOnMap: PropTypes.func.isRequired
};