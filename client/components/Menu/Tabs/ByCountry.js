'use strict';
import React, {PropTypes} from 'react';
import Autosuggest from 'react-autosuggest';
import {
    getSuggestions,
    getSuggestionValue,
    renderInputComponent,
    renderSuggestion,
    renderSuggestionsContainer
} from './Suggestion/Suggestion';
import {fetchJson} from '../../../utils';
import AnimalList from './AnimalList';

let data = [];

export default class ByCountry extends React.Component {
    constructor() {
        super();
        this.state = {suggestions: []};
        if (data.length === 0) fetchJson('api/countries', json => data = json);
    }

    onChange = (event, {newValue}) => {
        this.props.onValueChange(newValue);
    };

    onSuggestionsFetchRequested = ({value}) => {
        this.setState({
            suggestions: getSuggestions(value, data)
        });
    };

    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    loadNext(shown, data, offset) {
        if (offset + 10 >= data.length)
            this.props.loadNext(shown, data[offset + 9].binomial);
        this.props.onOffsetChange(offset + 10);
        console.log(data.length);
    };

    getItems(data, offset) {
        return data.slice(offset, offset + 10);
    }

    render() {
        const {suggestions} = this.state;
        const {value, shown, data, onItemClick, offset, onSubmit, onOffsetChange} = this.props;
        const items = this.getItems(data, offset);
        const inputProps = {
            placeholder: 'Type a binomial',
            value,
            onChange: this.onChange
        };
        return (
            <div>
                <div className='control'>
                    <label className='label'>Country:
                        <Autosuggest
                            suggestions={suggestions}
                            onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                            onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                            getSuggestionValue={getSuggestionValue}
                            renderInputComponent={renderInputComponent}
                            renderSuggestion={renderSuggestion}
                            renderSuggestionsContainer={renderSuggestionsContainer}
                            inputProps={inputProps}
                        />
                    </label>
                    <button className='button is-primary' onClick={() => onSubmit(value)}>
                        Find living in country
                    </button>
                </div>
                <nav className="pagination">
                    <button
                        onClick={() => onOffsetChange(offset - 10)}
                        disabled={offset - 10 < 0}
                        hidden={data.length == 0}>
                        Previous
                    </button>
                    <button
                        onClick={() => this.loadNext(shown, data, offset)}
                        hidden={data.length == 0}
                        disabled={offset + 10 > data.length}>
                        Next
                    </button>
                </nav>
                <AnimalList items={items}
                            onAnimalItemClick={(item) => onItemClick(shown, item.intersections)}/>
            </div>
        );
    }
}

ByCountry.PropTypes = {
    value: PropTypes.string.isRequired,
    shown: PropTypes.string.isRequired,
    offset: PropTypes.number.isRequired,
    data: PropTypes.array.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onItemClick: PropTypes.func.isRequired,
    onValueChange: PropTypes.func.isRequired,
    onOffsetChange: PropTypes.func.isRequired,
};