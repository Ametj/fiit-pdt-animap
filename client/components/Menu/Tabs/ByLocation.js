'use strict';
import React, {PropTypes} from 'react';
import AnimalList from './AnimalList';

let asked = false;

export default class ByLocation extends React.Component {
    constructor() {
        super();
    }

    getItems(data, offset) {
        return data.slice(offset, offset + 10);
    }

    render() {
        const {lat, long, offset, data, onSubmit, onLatChange, onLongChange, onOffsetChange, onItemClick} = this.props;
        const items = this.getItems(data, offset);

        if (!asked) navigator.geolocation.getCurrentPosition(function (position) {
            onLatChange(position.coords.latitude);
            onLongChange(position.coords.longitude);
            asked = true;
        });
        return (
            <div>
                <div className='control'>
                    <label className='label'>Latitude:
                        <input className='input' type='number' step='1' min='-90' max='90' name='lat' value={lat}
                               onChange={(event) => onLatChange(event.target.value)}/>
                    </label>
                    <label className='label'>Longitude:
                        <input className='input' type='number' step='1' min='-180' max='180' name='long'
                               value={long}
                               onChange={(event) => onLongChange(event.target.value)}/>
                    </label>
                    <button className='button is-primary' onClick={() => onSubmit(lat, long)}>
                        Find living nearby
                    </button>
                </div>
                <nav className="pagination">
                    <button
                        onClick={() => onOffsetChange(offset - 10)}
                        disabled={offset - 10 < 0}
                        hidden={data.length == 0}>
                        Previous
                    </button>
                    <button
                        onClick={() => onOffsetChange(offset + 10)}
                        disabled={offset + 10 >= data.length}
                        hidden={data.length == 0}>
                        Next
                    </button>
                </nav>
                <AnimalList items={items}
                            onAnimalItemClick={(item) => onItemClick(item.binomial)}/>
            </div>
        );
    }
}

ByLocation.PropTypes = {
    lat: PropTypes.string.isRequired,
    long: PropTypes.string.isRequired,
    offset: PropTypes.number.isRequired,
    data: PropTypes.array.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onItemClick: PropTypes.func.isRequired,
    onLatChange: PropTypes.func.isRequired,
    onLongChange: PropTypes.func.isRequired,
    onOffsetChange: PropTypes.func.isRequired,
};