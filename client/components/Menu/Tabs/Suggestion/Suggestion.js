'use strict';
import React from 'react';
import IsolatedScroll from 'react-isolated-scroll';
import Suggestion from './Suggestion.scss';

function escapeRegexCharacters(str) {
    return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

export function getSuggestions(value, data) {
    const escapedValue = escapeRegexCharacters(value.trim());

    if (escapedValue === '') {
        return [];
    }

    const regex = new RegExp('^' + escapedValue, 'i');

    return data.filter(data => regex.test(data.text));
}

export const getSuggestionValue = suggestion => suggestion.text;

export const renderSuggestion = suggestion => (
    <span>{suggestion.text}</span>
);

export const renderInputComponent = inputProps => (
    <div>
        <input {...inputProps} className='input'/>
    </div>
);

export function renderSuggestionsContainer({ref, ...rest}) {
    const callRef = isolatedScroll => {
        if (isolatedScroll !== null) {
            ref(isolatedScroll.component);
        }
    };

    return (
        <IsolatedScroll {...rest} ref={callRef}/>
    );
}
