'use strict';
import React, {PropTypes} from 'react';
import Header from '../Header';
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs';
import ByBinomialContainer from '../../containers/ByBinomialContainer';
import ByCountryContainer from '../../containers/ByCountryContainer';
import ByLocationContainer from '../../containers/ByLocationContainer';
import Style from './Menu.scss';

export default class Menu extends React.Component {
    constructor() {
        super();
        this.state = {selectedTab: 0};
    }

    handleSelect = (index) => {
        this.setState({selectedTab: index});
    };

    render() {
        return (
            <div className='column is-2'>
                <Header />
                <Tabs className='menu is-centered'
                      onSelect={this.handleSelect}
                      selectedIndex={this.state.selectedTab}>
                    <TabList>
                        <Tab>By binomial</Tab>
                        <Tab>By country</Tab>
                        <Tab>By location</Tab>
                    </TabList>
                    <TabPanel>
                        <ByBinomialContainer/>
                    </TabPanel>
                    <TabPanel>
                        <ByCountryContainer/>
                    </TabPanel>
                    <TabPanel>
                        <ByLocationContainer/>
                    </TabPanel>
                </Tabs>
            </div>
        );
    }
}