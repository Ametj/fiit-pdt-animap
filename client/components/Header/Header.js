'use strict';
import React from 'react';
import Style from './Header.scss';
import Title from '../Title';

export default class Header extends React.Component {
    constructor() {
        super();

        this.state = {
            title: 'Map App',
            subtitle: 'Map application written in React'
        }
    }

    render() {
        return (
            <header className='hero is-primary is-bold'>
                <div className='hero-body'>
                    <div className='container'>
                        <Title type='title' text='AniMap'/>
                        <Title type='subtitle' text='Animals on map.'/>
                    </div>
                </div>
            </header>
        );
    }
}