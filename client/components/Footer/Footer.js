'use strict';
import React from 'react';
import Style from './Footer.scss';

export default class Footer extends React.Component {
    render() {
        const year = new Date().getFullYear();

        return (
            <footer className='footer'>
                <div className='content has-text-centered'>
                    <p>
                        <strong>AniMap by Matej Leško </strong>
                        &#169; {year}
                    </p>
                </div>
            </footer>
        );
    }
}