'use strict';
import React, {PropTypes} from 'react'
import {createFeature, createFeatureCollection, createGeojson, createLayer} from './MapUtils';
import extent from 'turf-extent';
import Mapboxgl from 'mapbox-gl/dist/mapbox-gl';
import Style from './Map.scss';

export default class Map extends React.Component {
    constructor(props) {
        super(props);
    }

    setupLayers() {
        this.setupLayer('1', '#0f0'); // Extant
        this.setupLayer('2', '#af4'); // Probably extant
        this.setupLayer('3', '#af4'); // Possibly extant
        this.setupLayer('4', '#f80'); // Possibly extinct
        this.setupLayer('5', '#f00'); // Extinct
        this.setupLayer('6', '#000'); // Not Mapped

        this.map.addSource('points', createGeojson());
        this.map.addLayer({
            'id': 'points',
            'type': 'symbol',
            'source': 'points',
            'layout': {
                'icon-image': 'star-15',
            }
        });
    }

    setupLayer(id, color) {
        this.map.addSource(id, createGeojson());
        this.map.addLayer(createLayer(id, color));
    }

    updateSource(id, data) {
        //console.log(`Updating source: ${id}`);
        this.map.getSource(id).setData(data);
    }

    removeSource(id) {
        this.map.removeSource(id);
    }

    componentDidMount() {
        Mapboxgl.accessToken = 'pk.eyJ1IjoiYW1ldGoiLCJhIjoiY2l0eDdmYWo5MDAzZjJzazkyNzNmYW4zOSJ9.mQPvpq1ZIP8lIWv-3TB2FQ';
        this.map = new Mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/outdoors-v10',
            center: [48, 17],
            zoom: 2,

        });
        // disable map rotation using right click + drag
        this.map.dragRotate.disable();
        // disable map rotation using touch rotation gesture
        this.map.touchZoomRotate.disableRotation();

        this.map.on('load', () => this.setupLayers());
    }

    componentWillReceiveProps(nextProps) {
        const {data, marker} = nextProps;
        const fullData = createFeatureCollection();
        console.log('Updating map with data:', data);

        let geoData = Array.apply(null, Array(6)).map(createFeatureCollection);
        for (let i in data) {
            const coords = JSON.parse(data[i].coordinates);
            const idx = JSON.parse(data[i].presence) - 1;
            if (coords == null) continue;
            coords.forEach(function (polygon) {
                const feature = createFeature('Polygon', polygon);
                geoData[idx].features.push(feature);
                fullData.features.push(feature);
            });
        }
        for (let i = 0; i < geoData.length; i++) {
            this.updateSource((i + 1).toString(), geoData[i]);
        }
        if (fullData.features.length > 0) {
            const bbox = extent(fullData);
            this.map.fitBounds(bbox, {padding: 100});
        }

        const points = createFeatureCollection();
        if (marker.length > 0) {
            points.features.push(createFeature('Point', [marker[1], marker[0]]));
        }
        this.updateSource('points', points);
    }

    render() {
        return (
            <div className='column is-10'>
                <div id='map' className='map-container'/>
            </div>
        );
    }
}

Map
    .PropTypes = {
    data: PropTypes.array.isRequired,
    marker: PropTypes.array.isRequired
};