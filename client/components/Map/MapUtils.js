"use strict";

export function createGeojson() {
    return {
        'type': 'geojson',
        'data': {'type': 'FeatureCollection', 'features': []}
    }
}

export function createFeatureCollection() {
    return {'type': 'FeatureCollection', 'features': []};
}

export function createLayer(id, color) {
    return {
        'id': id,
        'type': 'fill',
        'source': id,
        'layout': {},
        'paint': {
            'fill-color': color,
            'fill-opacity': 0.75
        }
    };
}

export function createFeature(type, coordinates) {
    return {
        'type': 'Feature',
        'geometry': {
            'type': type,
            'coordinates': coordinates
        },
    };
}