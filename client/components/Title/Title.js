'use strict';
import React, { PropTypes } from 'react';

const Title = ({ type, text }) => <p className={type}>{text}</p>;

Title.propTypes = {
    type: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired
};

export default Title;