'use strict';
import {SHOW_POLYGON, GET_BY_COUNTRY, GET_BY_LOCATION, GET_BY_COUNTRY_NEXT, SHOW_MARKER} from './actionTypes';
import {fetchJson} from '../utils';

const showPolygon = data => ({
    type: SHOW_POLYGON,
    data
});

export const showMarker = (marker) => ({
    type: SHOW_MARKER,
    marker: marker
});

const getByCountry = (country, data) => ({
    type: GET_BY_COUNTRY,
    shown: country,
    data
});

const getByCountryNext = (country, data) => ({
    type: GET_BY_COUNTRY_NEXT,
    data
});

const getByLocation = data => ({
    type: GET_BY_LOCATION,
    data
});

export const onChange = (type, data) => ({
    type: type,
    data
});

export function fetchMammal(binomial, presence = '') {
    return dispatch => {
        return fetchJson(
            `/api/mammal?name=${binomial}&presence=${presence}`,
            json => dispatch(showPolygon(json)));
    }
}

export function fetchMammalByCountryIntersection(country, intersections) {
    return dispatch => {
        return fetchJson(
            `/api/mammal/intersections?country=${country}&data=${intersections.join(',')}`,
            json => dispatch(showPolygon(json)));
    }
}

export function fetchByCountry(country, offset = '') {
    return dispatch => {
        dispatch(showPolygon([]));
        dispatch(getByCountry('', []));
        return fetchJson(
            `/api/mammal/country?country=${country}&offset=${offset}`,
            json => dispatch(getByCountry(country, json)));
    }
}

export function fetchByCountryNext(country, offset = '') {
    return dispatch => {
        return fetchJson(
            `/api/mammal/country?country=${country}&offset=${offset}`,
            json => dispatch(getByCountryNext(country, json)));
    }
}

export function fetchByLocation(lat, long) {
    return dispatch => {
        dispatch(showPolygon([]));
        dispatch(getByLocation([]));
        return fetchJson(
            `/api/mammal/location?lat=${lat}&long=${long}`,
            json => dispatch(getByLocation(json)));
    }
}
