'use strict';
const favicon = require('serve-favicon');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const express = require('express');
const logger = require('morgan');
const path = require('path');

const webpack = require('webpack');
const devMiddleware = require('webpack-dev-middleware');
const hotMiddleware = require('webpack-hot-middleware');
const config = require('../webpack.config.js');

const index = require('./routes/index');
const api = require('./routes/api');

const app = express();
const compiler = webpack(config);

// view engine setup
app.set('views', path.join(__dirname, './views'));
app.set('view engine', 'hbs');

app.use(favicon(path.join(__dirname, '../public/images/favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

app.use(express.static(path.join(__dirname, '../public')));

app.use(devMiddleware(compiler, {
    publicPath: config.output.publicPath,
    historyApiFallback: true,
}));
app.use(hotMiddleware(compiler));

app.get('/', index);
app.get('/api/mammal/country', api.getByCountry);
app.get('/api/mammal/intersections', api.getMammalIntersectionsByCountry);
app.get('/api/mammal/location', api.getByLocation);
app.get('/api/mammal', api.getMammal);
app.get('/api/country', api.getCountry);
app.get('/api/mammals', api.getMammals);
app.get('/api/countries', api.getCountries);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers
app.use(function (err, req, res, next) {
    const status = err.status || 500;
    const message = app.get('env') === 'development'
        ? {message: err.message, status: status, error: err}
        : {message: err.message, status: status, error: {}};
    res.status(status);
    res.render('error', {title: 'OM Dashboard | Error', message: message});
});

module.exports = app;
