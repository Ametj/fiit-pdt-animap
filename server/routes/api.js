'use strict';
const dbf = require('../database');
const db = dbf.db;

function execQuery(req, res, next, query, arg = null) {
    db.any(query, arg)
        .then(function (data) {
            res.status(200)
                .send(data)
        })
        .catch(function (err) {
            return next(err);
        });
}

// GET /api/countries
function getCountries(req, res, next) {
    const query = `select name as text from countries`;
    execQuery(req, res, next, query);
}

// GET /api/mammals
function getMammals(req, res, next) {
    const query = `select distinct binomial as text from terrestrial_mammals`;
    execQuery(req, res, next, query);
}

// GET /api/country ?name=
function getCountry(req, res, next) {
    const query = `select coordinates from countries where name = $1`;
    const arg = [req.query.name];
    execQuery(req, res, next, query, arg);
}

// GET /api/mammal'?binomial || ?id, (?presence)
function getMammal(req, res, next) {
    const value = req.query.id || req.query.name;
    const param = (typeof req.query.id != 'undefined' && 'id_no') || 'binomial';
    let query = `select presence, coordinates from terrestrial_mammals where $1~ = $2`;
    const arg = [param, value];
    if (req.query.presence) {
        query += ` and presence = $3`;
        arg.push(parseInt(req.query.presence));
    }
    execQuery(req, res, next, query, arg);
}

// GET /api/mammal/country ?country, ?offset
function getByCountry(req, res, next) {
    const country = req.query.country;
    const offset = req.query.offset || '';
    const query = `select t.binomial, array_agg(distinct p.id) as intersections
            from mammals_polygons p join terrestrial_mammals t on p.gid = t.gid, countries c
            where t.presence = '1' and t.binomial > $1 and c.name = $2 and st_intersects(c.geom, p.geom)
            group by t.binomial order by t.binomial limit 10`;
    const arg = [offset, country];
    execQuery(req, res, next, query, arg);
}

// GET api/mammal/intersections ?country ?data
function getMammalIntersectionsByCountry(req, res, next) {
    const country = req.query.country;
    const data = req.query.data;
    const query = `select 1 as presence, (st_asgeojson(st_multi(st_intersection(c.geom, t.geom))))::json ->> 'coordinates' as coordinates
            from mammals_polygons t, countries c
            where t.id in ($2^) and c.name = $1`;
    const arg = [country, data];
    execQuery(req, res, next, query, arg);
}

// GET /api/mammal/location ?lat, ?long
function getByLocation(req, res, next) {
    const lat = req.query.lat;
    const long = req.query.long;
    const query = `select distinct t.binomial from mammals_polygons p join terrestrial_mammals t on p.gid = t.gid
            where t.presence = 1 and st_contains(p.geom, st_setsrid(st_makepoint($1, $2), 4326))`;
    const arg = [long, lat];
    execQuery(req, res, next, query, arg);
}

module.exports = {
    getMammal,
    getMammalIntersectionsByCountry,
    getCountry,
    getMammals,
    getCountries,
    getByCountry,
    getByLocation
};