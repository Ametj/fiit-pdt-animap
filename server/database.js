'use strict'
const pgp = require('pg-promise')();
const connection = 'postgres://postgres@localhost:5432/gisdb';
const db = pgp(connection);

module.exports = {db, pgp};